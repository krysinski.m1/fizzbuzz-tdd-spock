package m1.krysinski;

/**
 * Created by mich on 23.03.17.
 */
public class FizzBuzz {

    public String compute(int number){
        if(number < 1 || number > 100){
            throw new IllegalArgumentException();
        }
        if(number % 3 == 0 && number % 5 == 0){
            return "FizzBuzz";
        }else if(number % 5 == 0){
            return "Buzz";
        }else if(number % 3 == 0){
            return "Fizz";
        }else{
            return String.valueOf(number);
        }
    }

}
