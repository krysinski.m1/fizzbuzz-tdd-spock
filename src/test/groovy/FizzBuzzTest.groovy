/**
 * Created by mich on 23.03.17.
 */

import m1.krysinski.FizzBuzz
import spock.lang.*;

class FizzBuzzTest extends Specification{

    def fizzBuzz = new FizzBuzz()

    def "should raise InvalidArgumentException for 0"() {
        when:
            fizzBuzz.compute(0)

        then:
            thrown IllegalArgumentException
    }

    def "should return 1 for 1"() {
        when:
            def res = fizzBuzz.compute(1)
        then:
            res == "1"
    }

    def "should return Fizz for 3"() {
        when:
            def res = fizzBuzz.compute(3)
        then:
            res == "Fizz"
    }

    def "should return Buzz for 5"() {
        when:
            def res = fizzBuzz.compute(5)
        then:
            res == "Buzz"
    }

    def "should return FizzBuzz for 15"() {
        when:
            def res = fizzBuzz.compute(15)
        then:
            res == "FizzBuzz"
    }

    def "should return FizzBuzz for multiples of 15"(int n) {
        expect:
            fizzBuzz.compute(n) == "FizzBuzz"
        where:
            n  | _
            15 | _
            30 | _
            45 | _
            60 | _
            75 | _
            90 | _
    }

    def "should return Buzz for multiples of 5 not divisible by 3"() {
        expect:
            fizzBuzz.compute(n) == "Buzz"
        where:
            n << (1..100).findAll{x -> x % 5 == 0 && x % 3 != 0}
    }

    def "should return Fizz for multiples of 3 not divisible by 5"() {
        expect:
            fizzBuzz.compute(n) == "Fizz"
        where:
            n << (1..100).findAll{x -> x % 3 == 0 && x % 5 != 0}
    }

    def "should return number as string for numbers not divisible by 3 or 5"() {
        expect:
            fizzBuzz.compute(n) == String.valueOf(n)
        where:
            n << (1..100).findAll{x -> x % 3 != 0 && x % 5 != 0}
    }

    def "should raise IllegalArgumentException for numbers outside of <1, 100>"() {
        when:
            fizzBuzz.compute(n)
        then:
            thrown IllegalArgumentException
        where:
            n << (-100..200).findAll{x -> x < 1 || x > 100}

    }

}
